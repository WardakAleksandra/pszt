#include "maincontroller.h"
#include "mainwindow.h"
#include "evolutioncontroller.h"
#include "qdebug.h"
#include "village.h"
#include <QFileDialog>

const unsigned long MainController::NUMBER_OF_ALGORITHM_ITERATIONS = 5000;

MainController::MainController()
{

}

MainController::~MainController()
{

}

MainController& MainController::getInstance()
{
    static MainController instance;
    return instance;
}

void MainController::setWindow(MainWindow* window)
{
    this->window = window;
}

void MainController::createClans(int locationType)
{
    clansData = new ClansData(locationType);
    EvolutionController::getInstance().setClansData(*clansData);
}

void MainController::clean()
{
    delete clansData;
}

void MainController::drawMap(MainWindow * window) const
{
    qDebug() << "entered drawMap()";
    QVector<double> xAlly; //= {1.0, 5.0, 3.0}; //test values
    QVector<double>  yAlly; //= {1, 5, 3 };
    QVector<double>  xEnemy; //= {2, 9, 4 };
    QVector<double>  yEnemy; //= {4, 4, 0 };
    std::vector<Village*> villagesAlly = clansData->getClanAlly()->getVillages();

    for( Village* v : villagesAlly){
        xAlly.push_back(v->getX());
        yAlly.push_back(v->getY());
    }

    std::vector<Village*> villagesEnemy = clansData->getClanEnemy()->getVillages();

    for( Village* v : villagesEnemy){
        xEnemy.push_back(v->getX());
        yEnemy.push_back(v->getY());
    }

    window->drawMap(xAlly, yAlly, xEnemy, yEnemy);
}

void MainController::runEvolution() const
{

    EvolutionController::getInstance().setClansData(*clansData);
    std::vector<double> results = EvolutionController::getInstance().run(NUMBER_OF_ALGORITHM_ITERATIONS);

}

void MainController::runEvolution(int param)
{
    if (clansData == NULL)
    {
        return;
    }
    EvolutionController& evolutionController = EvolutionController::getInstance();
    evolutionController.setDefaultParams();

    switch(param)
    {
    // -----------------------DEFAULT PARAMS-----------------------------
    case 0:
        runEvolution();
        drawResult(window);
        break;
   // ------GENERATION_SIZE--------------------------------
    case 1:
    {
        std::string paramName = "Rozmiar populacji";
        std::vector<unsigned int> genSize_;
        genSize_.push_back(200);
        genSize_.push_back(100);
        genSize_.push_back(10);
        std::vector<unsigned int>::iterator it;
        for(it = genSize_.begin(); it != genSize_.end(); ++it){
            evolutionController.setGenerationSize(*it);
            runEvolution();
            drawResult(paramName, std::to_string(*it));
        }
    }
        break;
    //----------------------MUTATION_DECISION_THRESHOLD------------------------------------
    case 2:
    {
        std::string paramName = "Prawdopodobieństwo mutacji";
        std::vector<double> mutThresh_;
        mutThresh_.push_back(0.1);
        mutThresh_.push_back(0.5);
        mutThresh_.push_back(1.0);
        std::vector<double>::iterator it;
        for(it = mutThresh_.begin(); it != mutThresh_.end(); ++it){
            evolutionController.setMutationDecisionThreshold(*it);
            runEvolution();
            drawResult(paramName, std::to_string(*it));
        }
    }
        break;
    //----------------FITNESS_FUNTION_POWER-------------------------------------------
    case 3:
    {
        std::string paramName = "Wykładnik funkcji przystosowania";
        std::vector<double> power_;
        power_.push_back(2.0);
        power_.push_back(4.0);
        power_.push_back(6.0);
        std::vector<double>::iterator it;
        for(it = power_.begin(); it != power_.end(); ++it){
            evolutionController.setFitnessFunctionPower(*it);
            runEvolution();
            drawResult(paramName, std::to_string(*it));
        }
    }
        break;

    default:
        runEvolution();
        break;

    }


}

void MainController::drawResult(MainWindow * window) const
{
    std::vector<double> results = EvolutionController::getInstance().getBestResultFromEachGeneration();
    window->drawResult(QVector<double>::fromStdVector(results));
}

void MainController::drawResult(std::string paramName, std::string paramValue) const
{
    std::vector<double> results = EvolutionController::getInstance().getBestResultFromEachGeneration();
    window->drawResult(QVector<double>::fromStdVector(results), paramName, paramValue);
}

void MainController::loadResult(MainWindow * window) const
{
    std::string filter = "*.txt"; // zmienc na format, ktorego  bedziemy uzywac
    QString filename = QFileDialog::getOpenFileName(NULL, "Open...", "/home/", filter.c_str());
    //TODO wyciagnac z pliku wyniki ewolucji
    //filename: pełna ścieżka do wybranego pliku

//    std::vector<double> results = funckjaWyciagajacaWynik( filename.toStdString(););
    //    window->drawResult(QVector<double>::fromStdVector(results));
}

void MainController::saveResult() const
{
    std::string filter = "*.txt"; // zmienc na format, ktorego  bedziemy uzywac
    QString filename = QFileDialog::getSaveFileName(NULL, "Save...", "/home/",filter.c_str());

}

void MainController::displayGenerationParams(const int genNumber, const double genTime)
{
    window->displayGenerationParamas(genNumber, genTime);
}
