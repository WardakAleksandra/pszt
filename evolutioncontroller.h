#ifndef EVOLUTIONCONTROLLER_H
#define EVOLUTIONCONTROLLER_H

#include <vector>
#include <array>
#include <random>
#include "chromosome.h"
#include "clansdata.h"
#include "village.h"

class EvolutionController
{
public:
    static EvolutionController& getInstance();
    void setClansData(const ClansData& clansData); //TODO: moze zamiast setClansData przekazywac clansData w tej glownej funkcji zaczynajacej petle ewolucji? np. run(ClansData&)?
    void addChromosomeToCurrentGeneration( Chromosome chromosome);
    std::vector<double> run( unsigned long numberOfIterations);

    std::vector<double> getBestResultFromEachGeneration() const;

    void setGenerationSize(unsigned int const);
    void setMutationDecisionThreshold(double const);
    void setFitnessFunctionPower(double const);


    void setDefaultParams();

private:
    static const unsigned int NUMBER_OF_SELECTED_CHROMOSOME;
    static unsigned int GENERATION_SIZE;
    static double MUTATION_DECISION_THRESHOLD;
    static double FITNESS_FUNCTION_POWER;

    ~EvolutionController();
    EvolutionController();
    //EvolutionController(const ClansData& clansData);
    EvolutionController(const EvolutionController&) = delete;
    EvolutionController& operator=(const EvolutionController&) = delete;

    void evolution( unsigned long numberOfIterations);
    // Funkcja generująca początkowe chromosomy
    void initialization();
    // Funkcja wyliczająca wynik każdego chromosomu
    void generationEvaluation();
    double chromosomeCalculateTime( const Chromosome &chromosome);
    void selection();
    void newGenerationByCrossover();
    std::array<Chromosome, 2> crossover(const Chromosome &fistParent, const Chromosome &secondParent,
                                        unsigned long firstCrossoverPoint, unsigned long secondCrossoverPoint);
    void mutation();
    void mutateChromosome( Chromosome &chromosome);
    double calculateFitness(double time);

    typedef std::pair<Chromosome, double> ChromosomeAndFitness;

    ClansData mClansData_;
    std::vector<ChromosomeAndFitness> currentGeneration_;
    std::vector<ChromosomeAndFitness> selectedFromCurrentGeneration_;

    std::default_random_engine randomNumberGenerator_;

    std::vector<double> bestResultFromEachGeneration;



};

#endif // EVOLUTIONCONTROLLER_H
