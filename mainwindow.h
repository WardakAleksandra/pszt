#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "maincontroller.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void drawMap(const QVector<double>, const QVector<double>, const QVector<double>, const QVector<double>);
    void drawResult(const QVector<double>);
    void drawResult(const QVector<double>, std::string, std::string);
    void displayGenerationParamas(int const, double const);
    void clearGraphs();

private slots:
    void on_createClansButton_clicked();

    void on_evolutionButton_clicked();

    void on_resultsButton_clicked();

    void on_loadResultsButton_clicked();

private:
    Ui::MainWindow *ui;
    MainController& mainController = MainController::getInstance();
    int graphId;

};

#endif // MAINWINDOW_H
