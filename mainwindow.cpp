#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcustomplot.h"
#include "maincontroller.h"
#include "evolutioncontroller.h"
#include "qdebug.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mainController.setWindow(this);
    ui->map->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    graphId = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::drawMap(const QVector<double> xAlly, const QVector<double> yAlly,
                         const QVector<double> xEnemy, const QVector<double> yEnemy)
{
    qDebug() << "entered drawMap()";

    ui->map->clearGraphs();

    ui->map->addGraph();
    ui->map->graph(0)->setData(xAlly, yAlly);
    ui->map->graph(0)->setPen(QPen(Qt::blue));
    ui->map->graph(0)->setLineStyle(QCPGraph::LineStyle::lsNone);
    ui->map->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->map->graph(0)->setName("Wioski nasze");
    ui->map->graph(0)->addToLegend();


    ui->map->addGraph();
    ui->map->graph(1)->setData(xEnemy, yEnemy);
    ui->map->graph(1)->setPen(QPen(Qt::red));
    ui->map->graph(1)->setLineStyle(QCPGraph::LineStyle::lsNone);
    ui->map->graph(1)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->map->graph(1)->setName("Wioski wroga");
    ui->map->graph(1)->addToLegend();


    ui->map->xAxis->setLabel("X");
    ui->map->yAxis->setLabel("Y");
    ui->map->legend->setVisible(true);
    ui->map->rescaleAxes();
    ui->map->replot();

}

void MainWindow::drawResult(const QVector<double> time)
{
    qDebug() << "entered drawResult()";
    unsigned int generationNumber = time.size();
    std::vector<double> generations(generationNumber);
    std::iota (std::begin(generations), std::end(generations), 1); //fills vector with incremental values (0 .. generationNumber)



    ui->map->addGraph();
    ui->map->graph(0)->setData(QVector<double>::fromStdVector(generations), time);
    ui->map->graph(0)->setPen(QPen(Qt::red));
    ui->map->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
    ui->map->graph(0)->setName("Kampania z domyślnymi parametrami");
    //ui->map->graph(0)->setScatterStyle(QCPScatterStyle::ssCross);

    ui->map->yAxis->setLabel("CZAS [s]");
    ui->map->xAxis->setLabel("POKOLENIE");
    ui->map->rescaleAxes();
    ui->map->replot();
}

void MainWindow::drawResult(const QVector<double> time, std::string paramName, std::string paramValue)
{
    unsigned int generationNumber = time.size();
    std::vector<double> generations(generationNumber);
    std::iota (std::begin(generations), std::end(generations), 1); //fills vector with incremental values (0 .. generationNumber)
    if (graphId == 0)
    {
        QCPTextElement *legendTitle = new QCPTextElement(ui->map);
        legendTitle->setLayer(ui->map->legend->layer()); // place text element on same layer as legend, or it ends up below legend
        legendTitle->setText(paramName.c_str());
        legendTitle->setFont(QFont("sans", 9, QFont::Bold));
        legendTitle->setObjectName("title");
        // then we add it to the QCPLegend (which is a subclass of QCPLayoutGrid):
        if (ui->map->legend->hasElement(0, 0)) // if top cell isn't empty, insert an empty row at top
        {
          ui->map->legend->removeAt(0);
          ui->map->legend->insertRow(0);
        }
        ui->map->legend->addElement(0, 0, legendTitle); // place the text element into the empty cell
    }

    QPen pen;
    QColor colors[] = {Qt::darkRed, Qt::blue, Qt::darkYellow};
    pen.setColor(colors[graphId]);
    pen.setWidth(2);
    ui->map->addGraph();
    ui->map->graph(graphId)->setPen(pen);
    ui->map->graph(graphId)->setData(QVector<double>::fromStdVector(generations), time);
    ui->map->graph(graphId)->setLineStyle(QCPGraph::LineStyle::lsLine);
   // ui->map->graph(graphId)->setScatterStyle(QCPScatterStyle::ssCross);
    ui->map->graph(graphId)->setName(QString(paramValue.c_str()));
    ui->map->graph(graphId)->addToLegend();

    ui->map->yAxis->setLabel("CZAS [s]");
    ui->map->xAxis->setLabel("POKOLENIE");
    ui->map->legend->setVisible(true);
    ui->map->rescaleAxes();
    ui->map->replot();
    ++graphId;
}

void MainWindow::displayGenerationParamas(int const genNumber, double const genTime)
{
    ui->generationNumberField->setText(QString::number(genNumber));
    ui->timeField->setText(QString::number(genTime));
    ui->generationNumberField->repaint();
}

void MainWindow::clearGraphs()
{
    ui->map->clearGraphs();
    graphId = 0;
}

void MainWindow::on_createClansButton_clicked()
{
    int locationType = ui->villagesLocationComboBox->currentIndex();
    mainController.createClans(locationType);
    mainController.drawMap(this);
}

void MainWindow::on_evolutionButton_clicked()
{
    clearGraphs();
    mainController.runEvolution(ui->paramField->currentIndex());
}

void MainWindow::on_resultsButton_clicked()
{
    mainController.saveResult();
  //  mainController.drawResult(this);
}

void MainWindow::on_loadResultsButton_clicked()
{
    mainController.loadResult(this);
}
