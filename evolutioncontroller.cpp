#include "evolutioncontroller.h"
#include "maincontroller.h"
#include "math.h"
#include <algorithm>
#include <qdebug.h>

// Tymi dwoma rzeczami tez mozna machac
// MUTATION_DECISION_THRESHOLD od 0.0 do 1.0, im blizej 0.0 tym rzadziej mutacje
unsigned int EvolutionController::GENERATION_SIZE = 200;
double EvolutionController::MUTATION_DECISION_THRESHOLD = 0.01;
double EvolutionController::FITNESS_FUNCTION_POWER = 4.0;

const unsigned int EvolutionController::NUMBER_OF_SELECTED_CHROMOSOME = 2;

EvolutionController::EvolutionController(){
    randomNumberGenerator_.seed(std::random_device()());
}

void EvolutionController::setClansData(const ClansData& clansData)
{
    mClansData_ = clansData;
}

void EvolutionController::addChromosomeToCurrentGeneration(Chromosome chromosome)
{
    // Dodanie nowego chromosomu z wynikiem 0.0
    currentGeneration_.push_back(std::make_pair(chromosome, 0.0f));
}

std::vector<double> EvolutionController::run(unsigned long numberOfIterations)
{
    evolution(numberOfIterations);
    return bestResultFromEachGeneration;
    
}

EvolutionController& EvolutionController::getInstance()
{
    static EvolutionController instance;
    return instance;
}

EvolutionController::~EvolutionController()
{
}

void EvolutionController::evolution( unsigned long numberOfIterations){
    MainController& mainController = MainController::getInstance();
    bestResultFromEachGeneration.clear();
    initialization();
    for(unsigned long i=0; i<numberOfIterations;++i){
        generationEvaluation();
        selection();
        newGenerationByCrossover();
        mutation();
        mainController.displayGenerationParams(i, bestResultFromEachGeneration[i]);
    }
}

void EvolutionController::initialization()
{

    unsigned int enemyClanSize = mClansData_.getClanEnemy()->getClanSize();
    unsigned int alliedClanSize = mClansData_.getClanAlly()->getClanSize();
    std::uniform_int_distribution<unsigned int> attackOrderDistribution(0, alliedClanSize-1);
    currentGeneration_.clear();
    for(unsigned int i=0; i <GENERATION_SIZE; ++i){
        // Nowy chromosom z wynikiem 0.0
        ChromosomeAndFitness newChromosomeAndFitness = std::make_pair(Chromosome(), 0.0);
        // Losuje enemyClanSize liczb z przedzialu 0 do allyClanSize-1
        for(unsigned int j=0; j < enemyClanSize; ++j){
            unsigned int nextAlliedVillage = attackOrderDistribution(randomNumberGenerator_);
            newChromosomeAndFitness.first.addNextToOrder(nextAlliedVillage);
        }
        currentGeneration_.push_back(newChromosomeAndFitness);
    }
}


void EvolutionController::generationEvaluation()
{
    // Dla każdego chromosomu wylicz fitness i uaktualnij wartość w parze
    // Oprocz tego śledź najniższy osiągnięty w danej generacji czas, a na koniec zapisz go do najlepszych wyników
    double lowestTimeInGeneration = std::numeric_limits<double>::max();
    for( ChromosomeAndFitness &chromosomeAndFitness : currentGeneration_){
        double time = chromosomeCalculateTime(chromosomeAndFitness.first);
        chromosomeAndFitness.second = calculateFitness(time);
        if(lowestTimeInGeneration > time) lowestTimeInGeneration = time;
    }
    bestResultFromEachGeneration.push_back(lowestTimeInGeneration);

    // Funktor do porównywania chromosomów po wartości fitness (mozna uzyc zamiast tego wyrazen lambda)
    struct {
        bool operator ()(const ChromosomeAndFitness &firstPair, const ChromosomeAndFitness &secondPair) const
        {
            return firstPair.second < secondPair.second;
        }
    }compareFitness;

    std::sort(currentGeneration_.begin(), currentGeneration_.end(), compareFitness);
}

double EvolutionController::chromosomeCalculateTime(const Chromosome &chromosome)
{

    unsigned int enemyClanSize = chromosome.orderOfAlliedVillages_.size();
    unsigned int allyClanSize = mClansData_.getClanAlly()->getClanSize();
    std::vector<double> allyArmyAttackTimes(allyClanSize, 0.0);

    for(unsigned int enemyCityIndex=0; enemyCityIndex < enemyClanSize; ++enemyCityIndex){
        unsigned int alliedVillageIndex = chromosome.orderOfAlliedVillages_.at(enemyCityIndex);
        VillageAlly const * attackingAlliedVillage = static_cast<VillageAlly const *>(mClansData_.getClanAlly()->getVillagePointer(alliedVillageIndex));
        int enemyVillageX = mClansData_.getClanEnemy()->getVillagePointer(enemyCityIndex)->getX();
        int enemyVillageY = mClansData_.getClanEnemy()->getVillagePointer(enemyCityIndex)->getY();
        int alliedVillageX = attackingAlliedVillage->getX();
        int alliedVillageY = attackingAlliedVillage->getY();

        double alliedUnitSpeed = static_cast<double>(attackingAlliedVillage->getUnitSpeed());
        double distance = sqrt(pow(static_cast<double>(enemyVillageX-alliedVillageX), 2.0) + pow(static_cast<double>(enemyVillageY - alliedVillageY), 2.0));
        // Zakładamy, że każda armia musi wrócić, więc mnożym czas razy 2
        allyArmyAttackTimes.at(alliedVillageIndex) += 2.0*(distance/alliedUnitSpeed);
    }

    return *(std::max_element(allyArmyAttackTimes.begin(), allyArmyAttackTimes.end()));

}

void EvolutionController::selection()
{
    selectedFromCurrentGeneration_.clear();
    // Selekcja z prawdopodobieństwem proporcjonalnym do wartości fitness.
    // Wektor currentGeneration musi byc posortowany malejaco
    for( unsigned int i=0; i < NUMBER_OF_SELECTED_CHROMOSOME; ++i){
        double fitnessSum = 0.0f;
        for( ChromosomeAndFitness chromosomeAndFitness : currentGeneration_){
            fitnessSum += chromosomeAndFitness.second;
        }
        std::uniform_real_distribution<double> fitnessDistribution(0.0, fitnessSum);
        double value = fitnessDistribution(randomNumberGenerator_);
        std::vector<ChromosomeAndFitness>::iterator it;
        for(it = currentGeneration_.begin(); it != currentGeneration_.end(); ++it){
            value -= (*it).second;
            if(value <= 0.0f){
                break;
            }
        }
        selectedFromCurrentGeneration_.push_back(*it);
        currentGeneration_.erase(it);
    }
}

void EvolutionController::newGenerationByCrossover()
{
    currentGeneration_.clear();
    currentGeneration_ = selectedFromCurrentGeneration_;
    std::uniform_int_distribution<unsigned long> crossoverPointDistribution(0, mClansData_.getClanEnemy()->getClanSize()-1);
    while( currentGeneration_.size() < GENERATION_SIZE)
    {
        unsigned long firstCrossoverPoint = crossoverPointDistribution(randomNumberGenerator_);
        unsigned long secondCrossoverPoint = crossoverPointDistribution(randomNumberGenerator_);
        std::array<Chromosome, 2> newChromosomes = crossover(selectedFromCurrentGeneration_.at(0).first, selectedFromCurrentGeneration_.at(1).first, firstCrossoverPoint, secondCrossoverPoint);
        addChromosomeToCurrentGeneration(newChromosomes[0]);
        if(currentGeneration_.size() < GENERATION_SIZE)
        {
            addChromosomeToCurrentGeneration(newChromosomes[1]);
        }
    }
}

std::array<Chromosome, 2> EvolutionController::crossover(const Chromosome &firstParent, const Chromosome &secondParent, unsigned long firstCrossoverPoint, unsigned long secondCrossoverPoint)
{
    std::vector<unsigned long> firstChildOrderVector;
    std::vector<unsigned long> secondChildOrderVector;
    std::vector<unsigned long> firstParentOrderVector = firstParent.orderOfAlliedVillages_;
    std::vector<unsigned long> secondParentOrderVector = secondParent.orderOfAlliedVillages_;

    //Zamiana tak, żeby firstCrossoverPoint był zawsze mniejszy od secondCrossoverPoint
    if( firstCrossoverPoint > secondCrossoverPoint)
    {
        unsigned long temp = firstCrossoverPoint;
        firstCrossoverPoint = secondCrossoverPoint;
        secondCrossoverPoint = temp;
    }

    for( unsigned long i=0; i < mClansData_.getClanEnemy()->getClanSize(); ++i)
    {
        if ( i >= firstCrossoverPoint &&  i <= secondCrossoverPoint)
        {
            firstChildOrderVector.push_back(secondParentOrderVector.at(i));
            secondChildOrderVector.push_back(firstParentOrderVector.at(i));
        } else
        {
            firstChildOrderVector.push_back(firstParentOrderVector.at(i));
            secondChildOrderVector.push_back(secondParentOrderVector.at(i));
        }
    }

    return std::array<Chromosome, 2>{Chromosome(firstChildOrderVector), Chromosome(secondChildOrderVector)};
}

void EvolutionController::mutation()
{
    for(ChromosomeAndFitness &chromosomeAndFitness : currentGeneration_){
        mutateChromosome(chromosomeAndFitness.first);
    }
}

void EvolutionController::mutateChromosome( Chromosome &chromosome){
    std::uniform_real_distribution<double> tokenMutationDistribution(0.0,1.0);
    std::uniform_int_distribution<unsigned long> orderDistribution(0.0, mClansData_.getClanAlly()->getClanSize()-1);
    for( auto it = chromosome.orderOfAlliedVillages_.begin(); it != chromosome.orderOfAlliedVillages_.end(); ++it){
        // Losowanie czy pole ma być mutowane
        bool mutationDecision = tokenMutationDistribution(randomNumberGenerator_) < MUTATION_DECISION_THRESHOLD;
        if(mutationDecision)
        {
            (*it) = orderDistribution(randomNumberGenerator_);
        }
    }
}

double EvolutionController::calculateFitness(double time)
{
    //Tutaj można się bawić funkcją przystosowania, wazne zeby byla przeciwnie propocjonalna do time
    //double power = 4.0;
    double power = EvolutionController::FITNESS_FUNCTION_POWER;
    return double(std::pow(100.0,power)/(std::pow(time,power)));
}

void EvolutionController::setGenerationSize(const unsigned int generationSize_)
{
    GENERATION_SIZE = generationSize_;
}

void EvolutionController::setMutationDecisionThreshold(const double mutationDecisionThreshold)
{
    MUTATION_DECISION_THRESHOLD = mutationDecisionThreshold;
}

void EvolutionController::setFitnessFunctionPower(const double fitnessFunctionPower)
{
    FITNESS_FUNCTION_POWER = fitnessFunctionPower;
}

void EvolutionController::setDefaultParams()
{
    EvolutionController::GENERATION_SIZE = 200;
    EvolutionController::MUTATION_DECISION_THRESHOLD = 0.01;
    EvolutionController::FITNESS_FUNCTION_POWER = 4.0;
}

std::vector<double> EvolutionController::getBestResultFromEachGeneration() const
{
    return bestResultFromEachGeneration;
}


