#include "village.h"

Village::Village(long x, long y) : x(x), y(y)
{

}

long Village::getX() const
{
    return x;
}

void Village::setX(long value)
{
    x = value;
}

long Village::getY() const
{
    return y;
}

void Village::setY(long value)
{
    y = value;
}
