#ifndef CLANABSTRACT_H
#define CLANABSTRACT_H
#include <vector>
#include "village.h"
#include "villageally.h"


class Clan
{
public:
    Clan();
    void addVillage(long x, long y);
    void addAlliedVillage(long x, long y, int unitSpeed);
    const std::vector<Village*> getVillages();
    unsigned long getClanSize() const;
    Village const *getVillagePointer(unsigned long villageIndex) const;
private:
    std::vector<Village*> villages;
};

#endif // CLANABSTRACT_H
