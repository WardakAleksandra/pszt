#include "clansdata.h"

ClansData::ClansData(int &locationType)
{
    initClanAlly(locationType, 1);
    initClanEnemy(locationType);
}

ClansData::ClansData()
{

}

ClansData::~ClansData()
{
    delete clanAlly;
    delete clanEnemy;
}

Clan *ClansData::getClanAlly() const
{
    return clanAlly;
}

void ClansData::setClanAlly(Clan *value)
{
    clanAlly = value;
}

Clan *ClansData::getClanEnemy() const
{
    return clanEnemy;
}

void ClansData::setClanEnemy(Clan *value)
{
    clanEnemy = value;
}


void ClansData::initClanAlly()
{
    clanAlly = new Clan;
    // Testowy zestaw klanów. Optymalny czas do 10.0 (5 klanów, do każdego w dwie strony)
    clanAlly->addAlliedVillage(0,20,1);
    clanAlly->addAlliedVillage(20,20,1);
    clanAlly->addAlliedVillage(40,20,1);
    clanAlly->addAlliedVillage(60,20,1);
    clanAlly->addAlliedVillage(80,20,1);
    // Koniec testowego zestawu

    // Przykładowy,losowy zestaw
//    clanAlly->addAlliedVillage(2,53,2);
//    clanAlly->addAlliedVillage(34,40,1);
//    clanAlly->addAlliedVillage(17,15,3);
//    clanAlly->addAlliedVillage(55,60,5);
//    clanAlly->addAlliedVillage(88,23,1);


}

void ClansData::initClanEnemy()
{
    clanEnemy = new Clan;
    // Testowy zestaw klanów. Optymalny czas do 10.0 (5 klanów, do każdego w dwie strony)
    clanEnemy->addVillage(1,20);
    clanEnemy->addVillage(21,20);
    clanEnemy->addVillage(41,20);
    clanEnemy->addVillage(61,20);
    clanEnemy->addVillage(81,20);
    // Koniec testowego zestawu


    // Przykładowy, losowy
//    clanEnemy->addVillage(2,1);
//    clanEnemy->addVillage(4,21);
//    clanEnemy->addVillage(5,31);
//    clanEnemy->addVillage(34,61);
//    clanEnemy->addVillage(7,81);
//    clanEnemy->addVillage(123,1);
//    clanEnemy->addVillage(6,21);
//    clanEnemy->addVillage(67,1);
//    clanEnemy->addVillage(36,81);
//    clanEnemy->addVillage(23,31);

}



// Testowy zestaw z jednakowa szybkoscia jednostek i testowaniem przypadku symetrycznego (z pozycji wroga)
void ClansData::initClanAlly(int testcase, int speed)
{
    clanAlly = new Clan;
     
    switch(testcase)
    {
    case 1:
        AllyTestcase_01(speed);
        break;
    case 2:
        AllyTestcase_02(speed);
        break;
    case 3:
        AllyTestcase_03();
        break;
    case 4:
        AllyTestcase_04();
        break;
    case 5:
        AllyTestcase_05(speed);
        break;
    case 6:
        AllyTestcase_05();
        break;
    default:
        initClanAlly();
        break;
    }
}


void ClansData::initClanEnemy(int testcase)
{
    clanEnemy = new Clan;
    
    switch(testcase)
    {
    case 1:
        EnemyTestcase_01();
        break;
    case 2:
        EnemyTestcase_02();
        break;
    case 3:
        EnemyTestcase_03();
        break;
    case 4:
        EnemyTestcase_04();
        break;
    case 5:
        EnemyTestcase_05();
        break;
    case 6:
        EnemyTestcase_05();
        break;    
    default:
        initClanEnemy();
        break;
    }
}


void ClansData::AllyTestcase_01(int speed)
{
    clanAlly->addAlliedVillage( 96, 72,  speed);
    clanAlly->addAlliedVillage(431,269,  speed);
    clanAlly->addAlliedVillage(258,141,  speed);
    clanAlly->addAlliedVillage( 25,359,  speed);
    clanAlly->addAlliedVillage(  7, 97,  speed);
}

void ClansData::AllyTestcase_02(int speed)
{
    clanAlly->addAlliedVillage(203,  60,  speed);
    clanAlly->addAlliedVillage(180, 267,  speed);
    clanAlly->addAlliedVillage(223,  41,  speed);
    clanAlly->addAlliedVillage(489, 150,  speed);
    clanAlly->addAlliedVillage(146, 157,  speed);
}

void ClansData::AllyTestcase_03()
{
    clanAlly->addAlliedVillage(217, 41, 1);
    clanAlly->addAlliedVillage( 84,  0, 1);
    clanAlly->addAlliedVillage(169,224, 1);
    clanAlly->addAlliedVillage(228,108, 1);
    clanAlly->addAlliedVillage(462,464, 3);
    clanAlly->addAlliedVillage(455,395, 1);
    clanAlly->addAlliedVillage(281,327, 1);
    clanAlly->addAlliedVillage(461,491, 3);
    clanAlly->addAlliedVillage(495,442, 3);
    clanAlly->addAlliedVillage(327,436, 1);
}

void ClansData::AllyTestcase_04()
{
    clanAlly->addAlliedVillage( 41, 17, 3);
    clanAlly->addAlliedVillage( 34, 50, 1);
    clanAlly->addAlliedVillage(119,124, 3);
    clanAlly->addAlliedVillage( 78,158, 3);
    clanAlly->addAlliedVillage(112,214, 1);
    clanAlly->addAlliedVillage(  5,295, 1);
    clanAlly->addAlliedVillage( 31,327, 1);
    clanAlly->addAlliedVillage( 61,391, 3);
    clanAlly->addAlliedVillage(145,442, 3);
    clanAlly->addAlliedVillage( 27,486, 3);
}


void ClansData::AllyTestcase_05()
{
    clanAlly->addAlliedVillage( 16, 17, 1);
    clanAlly->addAlliedVillage( 59, 50, 3);
    clanAlly->addAlliedVillage(119,124, 3);
    clanAlly->addAlliedVillage(153,158, 3);
    clanAlly->addAlliedVillage(212,214, 1);
    clanAlly->addAlliedVillage(255,270, 1);
    clanAlly->addAlliedVillage(306,302, 1);
    clanAlly->addAlliedVillage(361,366, 1);
    clanAlly->addAlliedVillage(420,417, 1);
    clanAlly->addAlliedVillage(452,461, 3);
}

void ClansData::AllyTestcase_05(int speed)
{
    clanAlly->addAlliedVillage( 16, 17, speed);
    clanAlly->addAlliedVillage( 59, 50, speed);
    clanAlly->addAlliedVillage(119,124, speed);
    clanAlly->addAlliedVillage(153,158, speed);
    clanAlly->addAlliedVillage(212,214, speed);
    clanAlly->addAlliedVillage(255,270, speed);
    clanAlly->addAlliedVillage(306,302, speed);
    clanAlly->addAlliedVillage(361,366, speed);
    clanAlly->addAlliedVillage(420,417, speed);
    clanAlly->addAlliedVillage(452,461, speed);
}


void ClansData::EnemyTestcase_01()
{
    clanEnemy->addVillage(203,  60);
    clanEnemy->addVillage(180, 267);
    clanEnemy->addVillage(223,  41);
    clanEnemy->addVillage(489, 150);
    clanEnemy->addVillage(146, 157);
}

void ClansData::EnemyTestcase_02()
{
    clanEnemy->addVillage( 96, 72);
    clanEnemy->addVillage(431,269);
    clanEnemy->addVillage(258,141);
    clanEnemy->addVillage( 25,359);
    clanEnemy->addVillage(  7, 97);
}

void ClansData::EnemyTestcase_03()
{
    clanEnemy->addVillage(141,104);
    clanEnemy->addVillage(152,153);
    clanEnemy->addVillage( 42,132);
    clanEnemy->addVillage(171,216);
    clanEnemy->addVillage(218,145);
    clanEnemy->addVillage(197,226);
    clanEnemy->addVillage( 21, 38);
    clanEnemy->addVillage(119,162);
    clanEnemy->addVillage(417,299);
    clanEnemy->addVillage(285,394);
    clanEnemy->addVillage(453,311);
    clanEnemy->addVillage(322,333);
    clanEnemy->addVillage(423,414);
    clanEnemy->addVillage(391,461);
    clanEnemy->addVillage(253,368);
    clanEnemy->addVillage(297,394);
    clanEnemy->addVillage(412,257);
    clanEnemy->addVillage(287,359);
}


void ClansData::EnemyTestcase_04()
{
    clanEnemy->addVillage(491,  4);
    clanEnemy->addVillage(352, 53);
    clanEnemy->addVillage(492,132);
    clanEnemy->addVillage(371,166);
    clanEnemy->addVillage(418,245);
    clanEnemy->addVillage(397,276);
    clanEnemy->addVillage(421,338);
    clanEnemy->addVillage(419,362);
    clanEnemy->addVillage(367,449);
    clanEnemy->addVillage(435,494);
}

void ClansData::EnemyTestcase_05()
{
    clanEnemy->addVillage( 41, 29);
    clanEnemy->addVillage( 77, 78);
    clanEnemy->addVillage(142,132);
    clanEnemy->addVillage(196,191);
    clanEnemy->addVillage(243,245);
    clanEnemy->addVillage(297,276);
    clanEnemy->addVillage(346,338);
    clanEnemy->addVillage(394,387);
    clanEnemy->addVillage(442,449);
    clanEnemy->addVillage(485,494);
}
