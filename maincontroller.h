#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include "clansdata.h"
#include <string>
//#include "mainwindow.h"

class MainWindow;

class MainController
{
public:
    static MainController& getInstance();
    void setWindow(MainWindow* window);
    void createClans(int locationType);
    void clean();
    void drawMap(MainWindow*) const;
    void runEvolution() const;
    void runEvolution(int);
    void drawResult(MainWindow *) const;
    void drawResult(std::string, std::string) const;
    void loadResult(MainWindow *) const;
    void saveResult() const;
    void displayGenerationParams(int const, double const);

private:
    MainController();
    ~MainController();
    MainController(const MainController&) = delete;
    MainController& operator=(const MainController&) = delete;

    static const unsigned long NUMBER_OF_ALGORITHM_ITERATIONS;
    ClansData* clansData;
    MainWindow* window;
};

#endif // MAINCONTROLLER_H
