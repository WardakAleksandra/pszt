#ifndef CHROMOSOME_H
#define CHROMOSOME_H

#include <vector>
#include "villageally.h"

class Chromosome
{
public:
    Chromosome();
    Chromosome(std::vector<unsigned long> orderOfAlliedVillages);
    void addNextToOrder( unsigned long nextAlliedVillage);
    std::vector<unsigned long> orderOfAlliedVillages_;
};

#endif // CHROMOSOME_H
