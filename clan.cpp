#include "clan.h"

Clan::Clan()
{

}

void Clan::addVillage(long x, long y)
{
    villages.push_back(new Village(x,y));
}

void Clan::addAlliedVillage(long x, long y, int unitSpeed)
{
    villages.push_back(new VillageAlly(x,y,unitSpeed));
}

const std::vector<Village*> Clan::getVillages()
{
    return villages;
}

unsigned long Clan::getClanSize() const
{
    return villages.size();
}

 Village const * Clan::getVillagePointer(unsigned long villageIndex) const
{
    return villages.at(villageIndex);
}

