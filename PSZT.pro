#-------------------------------------------------
#
# Project created by QtCreator 2017-05-24T15:22:16
#
#-------------------------------------------------

QT       += core gui
CONFIG   += C++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = PSZT
TEMPLATE = app

LIBS += -LQtCore

SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    village.cpp \
    villageally.cpp \
    clan.cpp \
    clansdata.cpp \
    evolutioncontroller.cpp \
    chromosome.cpp \
    maincontroller.cpp

HEADERS  += mainwindow.h \
            qcustomplot.h \
    village.h \
    villageally.h \
    clan.h \
    clansdata.h \
    evolutioncontroller.h \
    chromosome.h \
    maincontroller.h

FORMS    += mainwindow.ui
