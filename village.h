#ifndef VILLAGEABSTRACT_H
#define VILLAGEABSTRACT_H


class Village
{
public:
    Village(long, long);

    long getX() const;
    void setX(long value);

    long getY() const;
    void setY(long value);

private:
    long x;
    long y;
};

#endif // VILLAGEABSTRACT_H
