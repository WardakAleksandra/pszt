#include "villageally.h"


VillageAlly::VillageAlly(long x, long y, unsigned int speed) : Village(x, y), unitSpeed(speed)
{
}

unsigned int VillageAlly::getUnitSpeed() const
{
    return unitSpeed;
}

void VillageAlly::setUnitSpeed(unsigned int speed)
{
    unitSpeed = speed;
}
