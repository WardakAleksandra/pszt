#ifndef CLANSDATA_H
#define CLANSDATA_H

#include "clan.h"

class ClansData
{
public:
    ClansData(int &locationType);
    ClansData();
    ~ClansData();


    Clan *getClanAlly() const;
    void setClanAlly(Clan *value);

    Clan *getClanEnemy() const;
    void setClanEnemy(Clan *value);

private:
    Clan* clanAlly;
    Clan* clanEnemy;
    void initClanAlly();
    void initClanEnemy();
    
    void initClanAlly(int testcase, int speed);
    void initClanEnemy(int testcase);
    void AllyTestcase_01(int speed);
    void AllyTestcase_02(int speed);
    void AllyTestcase_03();
    void AllyTestcase_04();
    void AllyTestcase_05();
    void AllyTestcase_05(int speed);
    void EnemyTestcase_01();
    void EnemyTestcase_02();
    void EnemyTestcase_03();
    void EnemyTestcase_04();
    void EnemyTestcase_05();
};

#endif // CLANSDATA_H
