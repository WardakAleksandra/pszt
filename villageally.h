#ifndef VILLAGEALLY_H
#define VILLAGEALLY_H

#include "village.h"

class VillageAlly : public Village
{
public:
    VillageAlly(long x, long y, unsigned int speed);

    unsigned int getUnitSpeed() const;
    void setUnitSpeed(unsigned int value);

private:
    unsigned int unitSpeed;
};

#endif // VILLAGEALLY_H
