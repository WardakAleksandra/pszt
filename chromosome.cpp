#include "chromosome.h"

Chromosome::Chromosome()
{

}

Chromosome::Chromosome(std::vector<unsigned long> orderOfAlliedVillages) : orderOfAlliedVillages_(orderOfAlliedVillages)
{
}

void Chromosome::addNextToOrder(unsigned long nextAlliedVillage)
{
    orderOfAlliedVillages_.push_back(nextAlliedVillage);
}
